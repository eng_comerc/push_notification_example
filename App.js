import React from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';
import { Permissions, Notifications } from 'expo';


export default class App extends React.Component {
  state = {
    token: ""
  }
  componentDidMount() {
    this.registerForPushNotificationsAsync();
  }

  async registerForPushNotificationsAsync() {
    const { status: existingStatus } = await Permissions.getAsync(
      Permissions.NOTIFICATIONS
    );
    let finalStatus = existingStatus;
  
    // only ask if permissions have not already been determined, because
    // iOS won't necessarily prompt the user a second time.
    if (existingStatus !== 'granted') {
      // Android remote notification permissions are granted during the app
      // install, so this will only ask on iOS
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      finalStatus = status;
    }
  
    // Stop here if the user did not grant permissions
    if (finalStatus !== 'granted') {
      return;
    }
  
    // Get the token that uniquely identifies this device
    let token = await Notifications.getExpoPushTokenAsync();
  
    console.log(token);
    alert(token);

    this.setState({
      token: token
    })

    _handleNotification = (notification) => {
      this.setState({notification: notification});
      alert(JSON.stringify(notification));
    };

    this._notificationSubscription = Notifications.addListener(this._handleNotification);
  }

  render() {
    return (
      <View style={styles.container}>
        <TextInput 
          value={this.state.token}
          />
        <Text>Open up App.js to start working on your app!</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
